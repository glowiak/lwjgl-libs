all:
	@echo lwjgl2 lwjgl3

lwjgl3:
	mkdir -p /usr/local/lib/lwjgl3
	cp -v ./x86_64/3/* /usr/local/lib/lwjgl3/

lwjgl2:
	mkdir -p /usr/local/lib/lwjgl
	cp -v ./x86_64/2/* /usr/local/lib/lwjgl/
